# add source files to container package
add_library(container OBJECT tree.cpp)

set(ALL_OBJECT_FILES
        ${ALL_OBJECT_FILES} $<TARGET_OBJECTS:container>
        PARENT_SCOPE)