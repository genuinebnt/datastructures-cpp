#!/bin/bash

# Ensure that you have the Conan and CMake commands in your PATH
# Install Conan: https://docs.conan.io/en/latest/installation.html
# Install CMake: https://cmake.org/download/

BUILD_TYPE="Debug"
BUILD_FOLDER="./build"
CMAKE_TOOLCHAIN_FILE="${BUILD_FOLDER}/build/${BUILD_TYPE}/generators/conan_toolchain.cmake"
CMAKE_BUILD_GENERATOR="Unix Makefiles"

mkdir -p $BUILD_FOLDER

conan install . --output-folder=$BUILD_FOLDER --profile=default --settings=build_type=${BUILD_TYPE} --build=missing

cd $BUILD_FOLDER || exit
cmake .. -G "$CMAKE_BUILD_GENERATOR" -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_TOOLCHAIN_FILE=$CMAKE_TOOLCHAIN_FILE
cmake --build .

cd ..